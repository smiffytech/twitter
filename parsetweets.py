#!/usr/bin/env python3

# Extract Twitter archive tweets.js to CSV.
#
# 2022-12-26 Matthew Smith <matt@smiffytech.com>
#
# License: CC BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/ 
#
# Works with Twitter archives as of November 2022.

import csv
import json
import os
import sys

def usage():
    print("Usage: parsetweets.py FILENAME", file=sys.stderr)
    sys.exit(-1)

# We should have exactly 1 argument (file to process.)
nargs = len(sys.argv)
if nargs != 2:
   usage() 

jfile = sys.argv[1]
# Does the input file exist?
if not os.path.exists(jfile):
    print("Input file '" + jfile + "' does not exist.", file=sys.stderr)
    sys.exit(-1)

fh = open(jfile, "r")
jsontxt = fh.read()
fh.close()

# Remove the initial JavaScript and parse the JSON.
jsontxt = jsontxt[26:]
jdata = json.loads(jsontxt)

# Output file is the same name and directory as the source, changed extension.
csvfile = jfile
csvfile = csvfile.replace(".js", ".csv")

# Twitter uses a horrible date format with words for months.
# This dictionary gives us an LUT to fix this.
months = {"Jan": "01",
    "Feb": "02",
    "Mar": "03",
    "Apr": "04",
    "May": "05",
    "Jun": "06",
    "Jul": "07",
    "Aug": "08",
    "Sep": "09",
    "Oct": "10",
    "Nov": "11",
    "Dec": "12"}

counter = 0

with open(csvfile, 'w', newline='') as csvfh:
    # dialect="unix" quotes all fields.
    csvwriter = csv.writer(csvfh, dialect="unix")

    # Add header row.
    row = [ "id",
        "created_at",
        "in_reply_to_status_id",
        "in_reply_to_screen_name",
        "full_text",
        "favourite_count",
        "retweet_count",
        "hashtags",
        "user_mentions",
        "urls"
        ]
    csvwriter.writerow(row)

    for tweetwrapper in jdata:
        counter += 1
        tweet = tweetwrapper["tweet"]

        # Grumble, grumble, fix the AWFUL date format.
        # Mon Nov 07 10:55:06 +0000 2022
        created_at = tweet["created_at"]
        chunks = created_at.split(" ")
        tstamp = chunks[5] + '-' + months[chunks[1]] + '-' + chunks[2] + ' ' + chunks[3]

        # This field doesn't occur, if not a reply.
        if "in_reply_to_status_id" in tweet:
            in_reply_to_status_id = tweet["in_reply_to_status_id"]
        else:
            in_reply_to_status_id = ""

        # This field also doesn't occur, if not a reply.
        if "in_reply_to_screen_name" in tweet:
            in_reply_to_screen_name = tweet["in_reply_to_screen_name"]
        else:
            in_reply_to_screen_name = ""

        # If there are hashtags, extract as a space-separated list.
        hashtags = tweet["entities"]["hashtags"]
        hbuffer = ""
        for hashtag in hashtags:
            hbuffer = hbuffer + hashtag["text"] + " "

        # Ditto for user screen names.
        user_mentions = tweet["entities"]["user_mentions"]
        umbuffer = ""
        for um in user_mentions:
            umbuffer = umbuffer + um["screen_name"] + " "

        # Ditto for URLs, which have been shortened by Twitter (t.co)
        urls = tweet["entities"]["urls"]
        urlbuffer = ""
        for url in urls:
            urlbuffer = urlbuffer + url["expanded_url"] + " "
        
        # Strip terminal spaces from buffers.
        hbuffer = hbuffer.rstrip()
        umbuffer = umbuffer.rstrip()
        urlbuffer = urlbuffer.rstrip()

        row = [tweet["id"],
            tstamp,
            in_reply_to_status_id,
            in_reply_to_screen_name,
            tweet["full_text"],
            tweet["favorite_count"],
            tweet["retweet_count"],
            hbuffer,
            umbuffer,
            urlbuffer]

        csvwriter.writerow(row)

csvfh.close()

print(str(counter) + " tweets written to " + csvfile)

sys.exit(0)
